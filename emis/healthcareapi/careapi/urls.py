from django.urls import path
from .views import (
    create_patient, update_patient, delete_patient,
    create_care_report, update_care_report, delete_care_report,
    create_doctor, update_doctor, delete_doctor,
)

urlpatterns = [
    # Patient URLs
    path('patients/', create_patient, name='create_patient'),
    path('patients/<int:patient_id>/', update_patient, name='update_patient'),
    path('patients/<int:patient_id>/delete/', delete_patient, name='delete_patient'),

    # Care Report URLs
    path('care-reports/', create_care_report, name='create_care_report'),
    path('care-reports/<int:care_report_id>/', update_care_report, name='update_care_report'),
    path('care-reports/<int:care_report_id>/delete/', delete_care_report, name='delete_care_report'),

    # Doctor URLs
    path('doctors/', create_doctor, name='create_doctor'),
    path('doctors/<int:doctor_id>/', update_doctor, name='update_doctor'),
    path('doctors/<int:doctor_id>/delete/', delete_doctor, name='delete_doctor'),
]