from django.apps import AppConfig


class CareapiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'careapi'
