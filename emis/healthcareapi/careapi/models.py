from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

class Patient(models.Model):
    GENDER_CHOICES = (('M','Male'),('F','Female'),('O','Other'))
    name = models.CharField(max_length=100)
    age = models.PositiveIntegerField()
    gender = models.CharField(max_length=1,choices=GENDER_CHOICES)
    contact_number = PhoneNumberField()
    email = models.EmailField()
    address = models.TextField()

    def __str__(self):
        return self.name


class Doctor(models.Model):
    name = models.CharField(max_length=100)
    specialization = models.CharField(max_length=100)
    contact_number = PhoneNumberField()
    email = models.EmailField()
    def __str__(self):
        return self.name

class CareReport(models.Model):
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    date = models.DateField()
    description = models.TextField()
    doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE)

    def __str__(self):
        return f"CareRecord for {self.patient.name} on {self.date}"