from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
# Create your views here.
from .models import Patient, CareReport, Doctor
from django.http import JsonResponse

@csrf_exempt
def create_patient(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        age = request.POST.get('age')
        gender = request.POST.get('gender')
        contact_number =    request.POST.get('contact_number')
        email = request.POST.get('email')
        address = request.POST.get('address')         
        # Additional fields for the patient model

        patient = Patient.objects.create(name=name, age=age,gender=gender,contact_number=contact_number,email=email,address=address)
        # Set additional fields for the patient model

        return JsonResponse({'message': 'Patient created successfully.'})

    return JsonResponse({'error': 'Invalid request method.'}, status=405)


@csrf_exempt
def update_patient(request, patient_id):
    try:
        patient = Patient.objects.get(pk=patient_id)
    except Patient.DoesNotExist:
        return JsonResponse({'error': 'Patient not found.'}, status=404)

    if request.method == 'PUT':
        name = request.PUT.get('name')
        age = request.PUT.get('age')
        gender = request.PUT.get('gender')
        contact_number =  request.PUT.get('contact_number')
        email = request.PUT.get('email')
        address = request.PUT.get('address') 
        # Additional fields for the patient model

        patient.name = name
        patient.age = age
        patient.gender = gender
        patient.contact_number = contact_number
        patient.email = email
        patient.address = address 
        # Update additional fields for the patient model

        patient.save()

        return JsonResponse({'message': 'Patient updated successfully.'})

    return JsonResponse({'error': 'Invalid request method.'}, status=405)


@csrf_exempt
def delete_patient(request, patient_id):
    try:
        patient = Patient.objects.get(pk=patient_id)
    except Patient.DoesNotExist:
        return JsonResponse({'error': 'Patient not found.'}, status=404)

    if request.method == 'DELETE':
        patient.delete()
        return JsonResponse({'message': 'Patient deleted successfully.'})

    return JsonResponse({'error': 'Invalid request method.'}, status=405)


@csrf_exempt
def create_care_report(request):
    if request.method == 'POST':
        patient_id = request.POST.get('patient_id')
        description = request.POST.get('description')
        # Additional fields for the care report model

        try:
            patient = Patient.objects.get(pk=patient_id)
        except Patient.DoesNotExist:
            return JsonResponse({'error': 'Patient not found.'}, status=404)

        care_report = CareReport.objects.create(patient=patient, description=description)
        # Set additional fields for the care report model

        return JsonResponse({'message': 'Care report created successfully.'})

    return JsonResponse({'error': 'Invalid request method.'}, status=405)


@csrf_exempt
def update_care_report(request, care_report_id):
    try:
        care_report = CareReport.objects.get(pk=care_report_id)
    except CareReport.DoesNotExist:
        return JsonResponse({'error': 'Care report not found.'}, status=404)

    if request.method == 'PUT':
        description = request.PUT.get('description')
        # Additional fields for the care report model

        care_report.description = description
        # Update additional fields for the care report model

        care_report.save()

        return JsonResponse({'message': 'Care report updated successfully.'})

    return JsonResponse({'error': 'Invalid request method.'}, status=405)


@csrf_exempt
def delete_care_report(request, care_report_id):
    try:
        care_report = CareReport.objects.get(pk=care_report_id)
    except CareReport.DoesNotExist:
        return JsonResponse({'error': 'Care report not found.'}, status=404)

    if request.method == 'DELETE':
        care_report.delete()
        return JsonResponse({'message': 'Care report deleted successfully.'})

    return JsonResponse({'error': 'Invalid request method.'}, status=405)


@csrf_exempt
def create_doctor(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        specialization = request.POST.get('specialization')
        contact_number =    request.POST.get('contact_number')
        email = request.POST.get('email')
        # Additional fields for the doctor model

        doctor = Doctor.objects.create(name=name,specialization=specialization,contact_number=contact_number,email=email)
        # Set additional fields for the doctor model

        return JsonResponse({'message': 'Doctor created successfully.'})

    return JsonResponse({'error': 'Invalid request method.'}, status=405)


@csrf_exempt
def update_doctor(request, doctor_id):
    try:
        doctor = Doctor.objects.get(pk=doctor_id)
    except Doctor.DoesNotExist:
        return JsonResponse({'error': 'Doctor not found.'}, status=404)

    if request.method == 'PUT':
        name = request.PUT.get('name')
        specialization = request.PUT.get('specialization')
        contact_number =  request.PUT.get('contact_number')
        email = request.PUT.get('email')
        # Additional fields for the doctor model

        doctor.name = name
        doctor.specialization = specialization
        doctor.contact_number = contact_number
        doctor.email = email
        # Update additional fields for the doctor model

        doctor.save()

        return JsonResponse({'message': 'Doctor updated successfully.'})

    return JsonResponse({'error': 'Invalid request method.'}, status=405)


@csrf_exempt
def delete_doctor(request, doctor_id):
    try:
        doctor = Doctor.objects.get(pk=doctor_id)
    except Doctor.DoesNotExist:
        return JsonResponse({'error': 'Doctor not found.'}, status=404)

    if request.method == 'DELETE':
        doctor.delete()
        return JsonResponse({'message': 'Doctor deleted successfully.'})

    return JsonResponse({'error': 'Invalid request method.'}, status=405)

def get_all_patients(request):
    patients = Patient.objects.all().values()
    return JsonResponse(list(patients), safe=False)

def get_patient_records(request, patient_id):
    records = CareReport.objects.filter(patient_id=patient_id).values()
    return JsonResponse(list(records), safe=False) 